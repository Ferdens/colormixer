//
//  ViewController.swift
//  CoreDataPlusTable
//
//  Created by Anton on 21.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate {
    
    let redColor    = "Red color: "
    let greenColor  = "Green color: "
    let blueColor   = "Blue color: "
    var context     : NSManagedObjectContext? = nil
    var appDelegate : AppDelegate?            = nil
    var entityData                            = [Any]()
    var redValue    : Float?
    var greenValue  : Float?
    var blueValue   : Float?
    
    @IBOutlet weak var blueColorLable   : UILabel!
    @IBOutlet weak var greenColorLable  : UILabel!
    @IBOutlet weak var redColorLable    : UILabel!
    @IBOutlet weak var mixView          : UIView!
    @IBOutlet weak var redColorSlider   : UISlider!
    @IBOutlet weak var greenColorSlider : UISlider!
    @IBOutlet weak var blueColorSlider  : UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initValue()
        self.setColor(red   : self.redValue!    ,
                      green : self.greenValue!  ,
                      blue  : self.blueValue!   )
        self.appDelegate = (UIApplication.shared.delegate) as! AppDelegate
        self.context  = self.appDelegate?.persistentContainer.viewContext
        
    }
    //MARK: My methods
    
    func initValue(){
        self.redValue   = self.redColorSlider.value
        self.greenValue = self.greenColorSlider.value
        self.blueValue  = self.blueColorSlider.value
    }
    func setLable(){
        self.redColorLable.text     = redColor   + String(Int(self.redValue! * 255.0))
        self.greenColorLable.text   = greenColor + String(Int(self.greenValue! * 255.0))
        self.blueColorLable.text    = blueColor  + String(Int(self.blueValue! * 255.0))
    }
    
    func loadDatatFromEntity(){
        do {
            self.entityData = try self.context!.fetch(Colors.fetchRequest())
        } catch {
            print("error")
        }
    }
    
    //MARK: Actions
    
    @IBAction func addColor(_ sender: UIButton) {
        self.loadDatatFromEntity()
        var isUnique = true
        for obj in entityData as! [Colors] {
            if (obj.red == Double(self.redColorSlider.value)) && (obj.green == Double(self.greenColorSlider.value)) && (obj.blue == Double(self.blueColorSlider.value)) {
                isUnique = false
            }
        }
        if isUnique {
            let color   = Colors(context: self.context!)
            color.red   = Double(self.redColorSlider.value)
            color.green = Double(self.greenColorSlider.value)
            color.blue  = Double(self.blueColorSlider.value)
            self.appDelegate?.saveContext()
        }
    }
    
    func setColor(red: Float,green: Float,blue: Float) {
        self.mixView.backgroundColor = UIColor.init(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1.0)
    }
    
    @IBAction func actionSlider(_ sender: UISlider) {
        self.initValue()
        self.setLable()
        self.setColor(red   : self.redValue!    ,
                      green : self.greenValue!  ,
                      blue  : self.blueValue!   )
    }
    
    
    
}

//
//  TableViewController.swift
//  CoreDataPlusTable
//
//  Created by Anton on 18.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {
    
    var context     : NSManagedObjectContext? = nil
    var appDelegate : AppDelegate? = nil
    var result      : [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appDelegate = (UIApplication.shared.delegate) as! AppDelegate
        self.context  = self.appDelegate?.persistentContainer.viewContext
        self.loadDataFromEntity()
    }
    
    @IBAction func tableEdit(_ sender: UIBarButtonItem) {
        if self.tableView.isEditing  {
            self.tableView.isEditing = false
        } else {
            self.tableView.isEditing = true
        }
    }
    
    func loadDataFromEntity(){
        do {
            self.result = try self.context!.fetch(Colors.fetchRequest())
        } catch {
            print("error")
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.result.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let data = result as! [Colors]
        let row = indexPath.row
        
        cell.backgroundColor = UIColor.init(red: CGFloat(data[row].red), green: CGFloat(data[row].green), blue: CGFloat(data[row].blue), alpha: 1)
        
        cell.textLabel?.text = " R:\( Int(data[row].red * 255.0)) G:\(Int(data[row].green * 255.0)) B:\(Int(data[row].blue * 255.0))"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            self.context?.delete(self.result[indexPath.row] as! NSManagedObject)
            self.result.remove(at: indexPath.row)
            do {
                try self.context?.save()
                
            } catch {
                print("error")
            }
            self.loadDataFromEntity()
            self.tableView.reloadData()
        }
    }
    
  
}
